This is pong. It requires python 2, and is not compatible with python 3.

It also requires [pygame](http://pygame.org/news.html).

To run it on linux or Mac, run:
python2 pong.py

To run it on Windows, simply use IDLE

To start the game, click inside the window.

This game can save one highscore!

Enjoy!
