#Tells it NOT to run the CMD friendly command
#This is an annotation
wait = False
#Makes it so the ball doesn't bounce immediatly starting the game
first_round = 100
#Makes pygame work
import pygame
import random
hs = 'highscores'
f = open(hs, 'r+')
if f.read() == "":
    f.write('0,No High Score!')
f.close()
# Define some colors
black    = (   0,   0,   0)
white    = ( 255, 255, 255)
green    = (   0, 255,   0)
red      = ( 255,   0,   0)
blue     = (   0,   0, 255)
ep = raw_input("Disco? (y/n) ")
epilipsy = False
if ep == 'y':
    epilipsy = True
    pygame.mixer.music.play(-1,0.0)
elif ep == 'g':
    color = green
else:
    epilipsy = False
pygame.init()
# Set the width and height of the screen [width,height]
size=[800,400]
screen=pygame.display.set_mode(size)
 
pygame.display.set_caption("Pong")
 
#Loop until the user clicks the close button.
done=False
 
# Used to manage how fast the screen updates
clock=pygame.time.Clock()

# Sets the paddle's speed and position
x_speed = 0
ry_speed = 0
x2_speed = 0
x3_speed = 0
rect_x = 400
rect_y = 350
rect2_x = 400
rect2_y = 50
# Sets the ball's starting position and speed
bx_speed = 1.25
by_speed = 1.25
ball_x = 375
ball_y = 175
# This class represents the paddle        
# It derives from the "Sprite" class in Pygame
class Block(pygame.sprite.Sprite):
     
    # Constructor. Pass in the color of the block, 
    # and its x and y position
    def __init__(self, color, width, height):
        # Call the parent class (Sprite) constructor
        pygame.sprite.Sprite.__init__(self) 
 
        # Create an image of the block, and fill it with a color.
        # This could also be an image loaded from the disk.
        self.image = pygame.Surface([width, height])
        self.image.fill(color)

        # Fetch the rectangle object that has the dimensions of the image
        # image.
        # Update the position of this object by setting the values 
        # of rect.x and rect.y
        self.rect = self.image.get_rect()
#Defines the paddle
paddle = pygame.sprite.RenderPlain()
player1 = Block(green,100,10)
player2 = Block(green,100,10)
paddle.add(player1)
paddle.add(player2)

class Sphere(pygame.sprite.Sprite):
     
    # Constructor. Pass in the color of the block, 
    # and its x and y position
    def __init__(self, color, width, height):
        # Call the parent class (Sprite) constructor
        pygame.sprite.Sprite.__init__(self) 
 
        # Create an image of the block, and fill it with a color.
        # This could also be an image loaded from the disk.
        self.image = pygame.Surface([width, height])
        self.image.fill(white)
        self.image.set_colorkey(white)
        pygame.draw.ellipse(self.image,color,[0,0,width,height])
 
        # Fetch the rectangle object that has the dimensions of the image
        # image.
        # Update the position of this object by setting the values 
        # of rect.x and rect.y
        self.rect = self.image.get_rect()
#Defines Ball
ball = Sphere(green, 25,25)
sphere = pygame.sprite.RenderPlain()
sphere.add(ball)
#Sets collision to False
blocks_hit_list = 0

#Resets the score
score = 0

#Import sound
bounce = pygame.mixer.Sound("Bounce.wav")

alive = pygame.mixer.music.load('alive.mp3')



smiles = pygame.image.load('smile.png')
# -------- Main Program Loop ----------
start = False
while start==False:
    for event in pygame.event.get():
        if event.type == pygame.MOUSEBUTTONDOWN:
            start = True
        elif event.type == pygame.QUIT:
            pygame.quit ()
    screen.fill(blue)
    pygame.draw.circle(screen, green, [375,175], 12, 0)
    pygame.draw.rect(screen, green, (400,350,100,10),0)
    pygame.draw.rect(screen, green, (400,50,100,10),0)
    pygame.display.flip()
while done==False:
    # ALL EVENT PROCESSING SHOULD GO BELOW THIS COMMENT
    for event in pygame.event.get(): # User did something   
        if event.type == pygame.QUIT:
            print "QUITTER!"
            wait=True
            done=True
         
    # User pressed down on a key
        if event.type == pygame.KEYDOWN:
            # Figure out if it was an arrow key. If so
            # adjust speed.
            if event.key == pygame.K_LEFT:
                x_speed=-2.5
            if event.key == pygame.K_RIGHT:
                x_speed=2.5
            if event.key == pygame.K_UP:
                ry_speed = -2.5
            if event.key == pygame.K_DOWN:
                ry_speed = 2.5
            if event.key == pygame.K_a:
                x2_speed=-2.5
            if event.key == pygame.K_d:
                x2_speed=2.5
            if event.key == pygame.K_w:
                x3_speed=-2.5
            if event.key == pygame.K_s:
                x3_speed = 2.5
             
    # User let up on a key
        if event.type == pygame.KEYUP:
            # If it is an arrow key, reset vector back to zero
            if event.key == pygame.K_LEFT:
                x_speed=0
            if event.key == pygame.K_RIGHT:
                x_speed=0
            if event.key == pygame.K_UP:
                ry_speed = 0
            if event.key == pygame.K_DOWN:
                ry_speed=0
            if event.key == pygame.K_a:
                x2_speed=0
            if event.key == pygame.K_d:
                x2_speed=0
            if event.key == pygame.K_w:
                x3_speed=0
            if event.key == pygame.K_s:
                x3_speed = 0
    # ALL EVENT PROCESSING SHOULD GO ABOVE THIS COMMENT
  
  
    # ALL GAME LOGIC SHOULD GO BELOW THIS COMMENT
    #Checks to see if the ball has collided with the paddle,
    #if so, bounce is true
    if pygame.sprite.spritecollide(player1, sphere, False) and first_round <= 0:
          blocks_hit_list = 1   #Says "THE BALL HIT THE PADDLE!"
          clock.tick(100000)
          bounce.play()  #Play bounce sound
    if pygame.sprite.spritecollide(player2, sphere, False) and first_round <= 0:
        blocks_hit_list = 1
        clock.tick(100000)
        bounce.play()
     
    # Check to see if the ball hit the paddle.
    if blocks_hit_list == 1:
          by_speed *= -1 * (random.random() + .5)  #Makes the ball bounce
          blocks_hit_list = 0   #Makes it so the ball cannot bounce again
          score += 1
          first_round = 100
    if ball_x <= 0 or ball_x >= 775:   #Checks if the ball hit the wall
        bx_speed *= -1 #Bounce
        clock.tick(100000)
        bounce.play() #Play sound
    if ball_y <= 0:
        print "Bottom Wins!"
        break
    #Did the ball pass the paddle?
    if ball_y >= 400:
        print "Top wins!"
        break
    randcolor = random.random()
    if randcolor < .2:
        color = black
    elif randcolor < .4:
        color = white
    elif randcolor < .6:
        color = red
    elif randcolor < .8:
        color = blue
    elif randcolor < 1:
        color = green
    # ALL GAME LOGIC SHOULD GO ABOVE THIS COMMENT
 
     
 
    # ALL CODE TO DRAW SHOULD GO BELOW THIS COMMENT
     
    # First, clear the screen to any color. Don't put other drawing commands
    # above this, or they will be erased with this command.
    if epilipsy == True:
        screen.fill(color)
        screen.blit(smiles,(100,-50))
    elif ep == 'g':
        screen.fill(green)
        screen.blit(smiles,(100,-50))
    else:
        screen.fill(blue)
    # Draw the paddle

    
    # Draw the ball
    ball.rect.x = ball_x
    ball.rect.y = ball_y
    sphere.draw(screen)
    # Move the rectangle
    rect_x += x_speed
    rect2_x += x2_speed
    rect2_y += x3_speed
    rect_y += ry_speed
    # Move the ball
    ball_x += bx_speed
    ball_y += by_speed
    # Draw the rectangle
    player1.rect.x = rect_x
    player1.rect.y = rect_y
    player2.rect.x = rect2_x
    player2.rect.y = rect2_y
    if rect_x <= 0:
        rect_x = 0
    if rect2_x <= 0:
        rect2_x = 0
    if rect_x >= 700:
        rect_x = 700
    if rect2_x >= 700:
        rect2_x = 700
    paddle.draw(screen)
    # Says GAME OVER

        
        
    # ALL CODE TO DRAW SHOULD GO ABOVE THIS COMMENT
     
    # Go ahead and update the screen with what we've drawn.
    pygame.display.flip()
 
    # FPS(Frames per second)
    clock.tick(240)
    #Makes the first round false so the ball bounces
    first_round = first_round - 1
     
# Close the window and quit.
# If you forget this line, the program will 'hang'
# on exit if running from IDLE.
pygame.quit ()
#Open file
f = open(hs, 'r+')
#Dump contents of file to scores string
scores = f.read()
#Turn scores into array
scores = scores.split(',')
#Print user score
print"Your score was:",score
#If user score lower than highscore, print highscore plus name
if int(scores[0]) >= score:
    print("Sorry! You didn't beat the high score, which is " + str(scores[0]) + " achieved by " + scores[1])
#Else, take name, clear the highscores file, and then save the new highscore and name
else:
    name = raw_input("You beat the high score! Enter your name to have your score saved: ")
    f.seek(0)
    f.truncate()
    f.write(str(score)+','+name)    
#Close game
raw_input("press enter or return to close")
